# This code is mean to demonstrate some common calculations done with GEOS-Chem-TOMAS code

# The first section of the code reads in monthly netCDF files. It is assumed these files were
# made using split_proc_all.pro (also available). Your files may be organized differently (especially
# if you did not use the split_proc_all.pro code. If so you can simply edit that section of the code -
# the variables names and general idea will still be the same.
# The second section of the code does common calculations such as changing units, getting total mass,
# calculating number and mass distributions, and CCN at 0.2% supersaturation. If you have further questions
# we are happy to help.

# Please follow GEOS-Chem guidlines regarding co-authorships, citations, and acknowledgments. 

# Written by: Jack Kodros, Jeff Pierce

###################
# Modules
###################
import datetime as dt
import matplotlib.pyplot as plt
import numpy as np
from netCDF4 import Dataset
import sys

#### -------------------------------------------------------------------------------####
#### MODEL output file names
#### -------------------------------------------------------------------------------####
# Filename prefix and dates of netcdf files
# Note: this is just for reading in a file. You may have a different file setup
dir = './'
prefix='gcT15'
mns=['20160101', '20160201', '20160301', '20160401', '20160501', '20160601'\
     , '20160701', '20160801', '20160901', '20161001', '20161101', '20161201']

#### -------------------------------------------------------------------------------####
#### Define TOMAS specifics
#### -------------------------------------------------------------------------------####

# Define number of TOMAS bins (here I'll assume 15 bins for TOMAS15)
nbins = 15 # number of bins

# List of TOMAS aerosol components
# [NK=number, SF=sulfate, SS=sea salt, ECIL="aged/mixed" black carbon", ECOB="fresh/un-mixed" BC
#             OCIL=hydrophilic OA, OCOB=hydrophobic OA, DUST=dust, AW=aerosol water]
sdhead = ['NK','SF','SS','ECIL','ECOB','OCIL','OCOB','DUST','AW']

# number of dry aerosol components (SF, SS, ECIL, ECOB, OCIL, OCOB, DUST)
ndry = 7 

# Molecular weights (SF, SS, ECIL, ECOB, OCIL, OCOB, DUST)
molwgt = [96., 58.5, 12., 12., 12., 12.,100.,18.] # [g/mol]

# Dry densities
dens = [1770.,2000.,2000.,1400.,1500.]  #kg m-3 [so4, ss, bc, oa, dust]

#### -------------------------------------------------------------------------------####
#### Define vertical grid from GEOS-Chem
#### -------------------------------------------------------------------------------####

# GEOS-5/GEOS-FP/MERRA reduced grid
nlevs=47
etae=np.array([0.000000,0.000055,0.000199,0.000601,0.001625,0.004026,\
      0.009191,0.019586,0.028077,0.039768,0.055820,0.077726,\
      0.091442,0.107578,0.126563,0.148896,0.175170,0.206167,\
      0.242774,0.285974,0.335486,0.373114,0.410759,0.448431,\
      0.486118,0.523819,0.561527,0.599251,0.636974,0.674708,\
      0.699867,0.725026,0.750186,0.775350,0.800515,0.820648,\
      0.835748,0.850848,0.865949,0.881051,0.896152,0.911253,\
      0.926356,0.941459,0.956562,0.971665,0.986769,1.001796])# box edge sigma coordinates
etam=np.array([0.000028,0.000127,0.000400,0.001113,0.002825,0.006609,\
      0.014389,0.023832,0.033923,0.047794,0.066773,0.084584,\
      0.099510,0.117070,0.137729,0.162033,0.190668,0.224471,\
      0.264374,0.310730,0.354300,0.391937,0.429595,0.467274,\
      0.504968,0.542673,0.580389,0.618113,0.655841,0.687287,\
      0.712447,0.737606,0.762768,0.787933,0.810582,0.828198,\
      0.843298,0.858399,0.873500,0.888601,0.903703,0.918805,\
      0.933908,0.949010,0.964113,0.979217,0.994283]) # box middle
psurf=1000 #mb
ptop=0.01 #mb

# make arrays of pressure
prese=[]
for ll in range(0,nlevs+1):
   l=nlevs-ll-1
   prese.append(ptop+etae[l]*(psurf-ptop))
prese=np.array(prese)

presm=[]
for ll in range(0,nlevs):
   l=nlevs-ll-1
   presm.append(ptop+etam[l]*(psurf-ptop))
presm=np.array(presm)

#### -------------------------------------------------------------------------------####
#### Read in monthly GEOS-Chem-TOMAS netCDF files and get TOMAS tracers
#### -------------------------------------------------------------------------------####
# NOTE: your file seup may be different
# Here we are just looping through monthly output (1 netCDF file per month) and
# taking out the TOMAS tracers (the IJ-AVG) diagnostic

data=[]
dist=[]
for d in range(0,len(mns)):
   fname=dir+prefix+'_'+mns[d]+'.nc'
   nc_file = Dataset(fname,'r')
   data = []
   for c in range(0,len(sdhead)):
      sizedist = []
      for k in range(0,nbins):
         sizedist.append(nc_file.variables['IJ_AVG_S__'+sdhead[c]+str(k+1)][:])
      data.append(sizedist)
   dist.append(data)
   lat=nc_file.variables['lat'][:]
   lon=nc_file.variables['lon'][:]
   nc_file.close()

# This array contains all the TOMAS tracers
# Dimensions are in [month, composition, size bin, level, lat, lon]
# Composition is in the order defined by 'sdhead'
dist = np.array(dist)[:,:,:,0,:,:,:]

# Make arrays of lat/lon
lat = np.array(lat)
lon = np.array(lon)

# Convert units of number to cm-3 STP
# (NOTE: geos-chem treats all tracers as ppb, so we consider number as ppb with a molwght of 1)
dist[:,0,:,:,:,:]=dist[:,0,:,:,:,:]/22.4e15*273./293. # comp, bin, level, lat, lon

# Convert mass species to ug m-3
for c in range(0,len(molwgt)):
   dist[:,c+1,:,:,:,:] = dist[:,c+1,:,:,:,:]*molwgt[c]/22.4e15*1e6*1E9

#### -------------------------------------------------------------------------------####
#### Examples
#### -------------------------------------------------------------------------------####
### In this section, I'll provide some examples of common uses of TOMAS output:
# - Get total number/mass concentrations
# - Calculate bin diameters
# - Plot number and volume distribution
# - Plot BC/OA mass distributions
# - Calculate number of particles greater than 3, 10, 40, and 80 nm

# For simplicity, lets take the time-average
dist = np.array(dist).mean(0)

##### -------- Get total N, M ---------------------
# In the previous section, we put all of the TOMAS tracers into one big array called "dist"
# This is just for convience, it is easy to write/code with just this one array.
# You can, of course, treat everything separately. Here I'll grab the total mass and number concentrations
# from the "dist" array. Recall, "dist" has dimemsions [composition, bin, level, lat, lon] (we averaged out time).
# For composition - we read in species in the order number, sulfate, sea salt, bc, oa, dust, aerosol water. So,
# the 0th index is number, the 1st is sulfate,...the last is aerosol water.
aeroNum = dist[0,:,:,:,:]                   # [cm-3] -- first composition is number
SO4 = dist[1,:,:,:,:]                       # [ug m-3] -- SO4
SS = dist[2,:,:,:,:]                        # [ug m-3] -- sea salt
BC = (dist[3,:,:,:,:] + dist[4,:,:,:,:])    # [ug m-3] -- internally mixed and externally mixed BC
OA = (dist[5,:,:,:,:] + dist[6,:,:,:,:])    # [ug m-3] -- hydrophilic and hydrophobic OA
DU = dist[7,:,:,:,:]                        # [ug m-3] -- dust
AW = dist[8,:,:,:,:]                        # [ug m-3] -- aerosol water

# These species now have dimensions [bin, level, lat, lon]. If you want total number or total BC mass,
# simply sum over the size bins
aeroNum_tot = aeroNum.sum(0)   # [cm-3] total aerosol number concentration
BC_tot = BC.sum(0)             # [ug m-3] total BC mass concentration

# Of course the benefit of TOMAS is the size resolution. Next, we calculate diameters and plot number/volume distributions

##### -------- Bin diameters ---------------------
# A common area of confusion with TOMAS comes with how the size bins are defined
# In TOMAS, the bin edges are defined based on the mass per particle.
# So in other words, bin 1 contains the total number of particles with mass greater than
# the lowest bin edge and less than the upper bin edge. The average mass per particle in bin 1
# is thus the total dry mass divided by the total number. To convert from mass per particle bin definitions
# to diameter bin edges there are two options: 
# Option 1 (a simplification):
# ----- Assume a constant particle density and calculate the diameter for a spherical particle for each bin
# ----- The benefit of this method is that it is quick and simple. All gridboxes will have the same bin-diameter definitions.
# ----- The drawback of this is that it is a simplification and the diameters are not "exact" (as in they aren't what is simulated)
# ----- This method may be sufficient if you do not need more specific diameter information.
#
# Option 2 (a more correct calculation):
# ----- Calculate dry particle density in each size bin in each gridcell
# ----- The benefit of this method is that you calculate the "exact" (or the "more-correct") particle diameters
# ----- The bin-diameter definitions will be different for every gridcell, as particle density varies spatially/temporally.
# ----- This method should be used in calculations that require a more-correct diameter.
#
# I will incude code for both methods here.

### Option 1 (simplification method)
if nbins == 15:
   xk = np.zeros(nbins+1)  # particle mass cutoffs [kg]...15 bins makes 16 bin *edges*

   rho_assumed = 1400. #[kg m-3] assumed density

   # First re-create the mass bin edges
   # This is defined for all TOMAS 15 simulations -- this part is not a simplification
   xk[0]=1.6E-23  # avg mass per particle in lowest bin edge   
   for k in range(1,nbins+1):
      if k < nbins-1:
         xk[k] = xk[k-1]*4.
      else:
         xk[k] = xk[k-1]*32.

elif nbins == 40:
   for k in range(1, nbins+1):
      xk[k] = xk[k-1]*2

# Calculate diameter based on assumed density -- this is a simplification
vol = xk[:]/rho_assumed
Dpk1 = (6.*vol/np.pi)**(1./3)*1e6 # particle diameter cutoffs [um]

# Now calculate diameter for the center of the bin
x = np.sqrt(xk[:-1]*xk[1:])
vol = x/rho_assumed
Dp1 = (6.*vol/np.pi)**(1./3)*1e6
   
### Option 2 (more-correct calculation)

# First lets grab total number and dry particle mass
# recall dist has dimensions [composition, bin, level, lat, lon] (we averaged out time)
aeroNum = dist[0,:,:,:,:]*1e6                    # [m-3] -- first composition is number
SO4 = dist[1,:,:,:,:]*1e-9                       # [kg m-3] -- SO4
SS = dist[2,:,:,:,:]*1e-9                        # [kg m-3] -- sea salt
BC = (dist[3,:,:,:,:] + dist[4,:,:,:,:])*1e-9    # [kg m-3] -- internally mixed and externally mixed BC
OA = (dist[5,:,:,:,:] + dist[6,:,:,:,:])*1e-9    # [kg m-3] -- hydrophilic and hydrophobic OA
DU = dist[7,:,:,:,:]*1e-9                        # [kg m-3] -- dust

dryMass = SO4[:,:,:,:] + SS[:,:,:,:] + BC[:,:,:,:] + OA[:,:,:,:] + DU[:,:,:,:]  #total dry mass [ug m-3]
tot_pvol = (SO4[:,:,:,:]/aeroNum[:,:,:,:]/dens[0]) + (SS[:,:,:,:]/aeroNum[:,:,:,:]/dens[1]) +\
       (BC[:,:,:,:]/aeroNum[:,:,:,:]/dens[2]) + (OA[:,:,:,:]/aeroNum[:,:,:,:]/dens[3]) +\
       (DU[:,:,:,:]/aeroNum[:,:,:,:]/dens[4])   #total volume per particle

dryMassPer = dryMass/aeroNum  #dry mass per particle

Dp = (6.*tot_pvol/np.pi)**(1.0/3.0)          # [m] average particle diameter of bin
Dp_lower = Dp[:,:,:,:]*(xk[:-1,np.newaxis,np.newaxis,np.newaxis]/dryMassPer[:,:,:,:])**(1.0/3.0)  #[m]
Dp_upper = Dp[:,:,:,:]*(xk[1:,np.newaxis,np.newaxis,np.newaxis]/dryMassPer[:,:,:,:])**(1.0/3.0)   #[m]

# Convert to microns
Dp = Dp*1e6               # [um]
Dp_lower = Dp_lower*1e6   # [um]
Dp_upper = Dp_upper*1e6   # [um]


##### -------- Number/Volume distribution ---------------------
# Okay, great, now that we have calculated the bin diameters, let's do something with them!
# Let's calculate and plot number/volume distributions for Fort Collins, Co

# Get number/mass (we've done this earlier, but I'll repeat it here)
aeroNum = dist[0,:,:,:,:]                 # [cm-3]
OA = (dist[5,:,:,:,:] + dist[6,:,:,:,:])  # [ug m-3]

# Coordinates of CSU atmospheric science department
latFoco = 40.59
lonFoco = -105.14

# Find gridcell
ilat = abs(lat - latFoco).argmin()
ilon = abs(lon - lonFoco).argmin()
ilev = 0  #surface 

DpFC = Dp[:,ilev,ilat,ilon]                #[um]
Dp_lowerFC  = Dp_lower[:,ilev,ilat,ilon]   #[um]
Dp_upperFC = Dp_upper[:,ilev,ilat,ilon]    #[um]
aeroNumFC = aeroNum[:,ilev,ilat,ilon]      #[cm-3]
OA_FC = OA[:,ilev,ilat,ilon]               #[ug m-3]

# Number distribution
dNdlog10Dp = aeroNumFC/np.log10(Dp_upperFC/Dp_lowerFC)   #[cm-3]
dVdlog10Dp = (np.pi/6. * DpFC**3 * aeroNumFC)/np.log10(Dp_upperFC/Dp_lowerFC)  #[um3 cm-3]
dMoadlog10Dp = OA_FC/np.log10(Dp_upperFC/Dp_lowerFC) # [ug m-3]

# Plot
# Here are some quick and simple plots 
#fig = plt.figure()
#plt.semilogx(DpFC, dNdlog10Dp)
#plt.xlabel('Diameter')
#plt.ylabel('dNdlog10Dp')
#plt.show()

#fig = plt.figure()
#plt.semilogx(DpFC, dVdlog10Dp)
#plt.xlabel('Diameter')
#plt.ylabel('dVdlog10Dp')
#plt.show()

#fig = plt.figure()
#plt.semilogx(DpFC, dMoadlog10Dp)
#plt.xlabel('Diameter')
#plt.ylabel('dMoadlog10Dp')
#plt.show()

##### -------- N3/N10/N40/N80 ---------------------
# Its common with TOMAS to use size classes to describe particle number concentration. Common ones
# in our group are N3, N10, N40, N80 ("Nx" is short hand for the number of particles with diameters
# greater than x nm"). N40 and N80 are typical CCN proxies. Since we are already using N40 as a CCN
# proxy, we'll go ahead and use the simple method for particle diameters.

# Particle number
aeroNum = dist[0,:,:,:,:]  #[cm-3]

# cutoff diameters 3nm, 10 nm, 40 nm, 80 nm
cut1 = 0.003  #[um]
cut2 = 0.01   #[um]
cut3 = 0.04   #[um]
cut4 = 0.08   #[um]

# We want to take all bins greater than the cutoff as well as some fraction of the bin that includes the cutoff
# For instance, if the 0th bin has edges of 2 and 4 nm, than N3 is the sum of bins 1-14 and approx half of bin 0. 
i = np.where(Dpk1 > cut1)[0]
frac = 1 - (cut1 - Dpk1[i[0]-1])/(Dpk1[i[0]] - Dpk1[i[0]-1])
N3 = aeroNum[i[0]-1,:,:,:]*frac + aeroNum[i[0]:,:,:,:].sum(0)

i = np.where(Dpk1 > cut2)[0]
frac = 1 - (cut2 - Dpk1[i[0]-1])/(Dpk1[i[0]] - Dpk1[i[0]-1])
N10 = aeroNum[i[0]-1,:,:,:]*frac + aeroNum[i[0]:,:,:,:].sum(0)

i = np.where(Dpk1 > cut3)[0]
frac = 1 - (cut3 - Dpk1[i[0]-1])/(Dpk1[i[0]] - Dpk1[i[0]-1])
N40 = aeroNum[i[0]-1,:,:,:]*frac + aeroNum[i[0]:,:,:,:].sum(0)

i = np.where(Dpk1 > cut4)[0]
frac = 1 - (cut4 - Dpk1[i[0]-1])/(Dpk1[i[0]] - Dpk1[i[0]-1])
N80 = aeroNum[i[0]-1,:,:,:]*frac + aeroNum[i[0]:,:,:,:].sum(0)

##### -------- Calculate CCN 0.2% ---------------------
# As one final example, let's calculate CCN at S=0.2% from the TOMAS output

### First, we calculate kappa of mixed particle
# Individual kappa for each species [so4, ss, bcint, bcext, oa hydrophilic, oa hydrophobic, dust]
kappa = np.array([1.,1.2,0.,0.,0.1,0.01,0.01])

# Mass concentrations (repeating here just for clarity) -- everything in SI units
aeroNum = dist[0,:,:,:,:]*1e6    # [m-3]
SO4 = dist[1,:,:,:,:]*1e-9       # [kg m-3]
SS = dist[2,:,:,:,:]*1e-9        # [kg m-3]
BCint = dist[3,:,:,:,:]*1e-9     # [kg m-3]
BCext = dist[4,:,:,:,:]*1e-9     # [kg m-3]
OAIL = dist[5,:,:,:,:]*1e-9      # [kg m-3] hydrophilic OA
OAOB = dist[6,:,:,:,:]*1e-9      # [kg m-3] hydrophobic OA
DUST = dist[7,:,:,:,:]*1e-9      # [kg m-3]
AW = dist[8,:,:,:,:]*1e-9        # [kg m-3] aerosol water

pVolSO4 = SO4/aeroNum/dens[0]      #[m3]
pVolSS = SS/aeroNum/dens[1]
pVolBCint = BCint/aeroNum/dens[2]
pVolBCext = BCext/aeroNum/dens[2]
pVolOAIL = OAIL/aeroNum/dens[3]
pVolOAOB = OAOB/aeroNum/dens[3]
pVolDUST = DUST/aeroNum/dens[4]

dVol = pVolSO4 + pVolSS + pVolBCint + pVolBCext + pVolOAIL + pVolOAOB + pVolDUST  # total volume [m3]

# Kappa of mixed particle in each bin
tkappa = (pVolSO4/dVol*kappa[0]) + (pVolSS/dVol*kappa[1]) + (pVolBCint/dVol*kappa[2]) + (pVolBCext/dVol*kappa[3])\
         + (pVolOAIL/dVol*kappa[4]) + (pVolOAOB/dVol*kappa[5]) + (pVolDUST/dVol*kappa[6])

# Calculate critical supersat at each bin edge
rhow = 1000  # density of water [kg m-3]
Mw = 0.018   # [kg mol-1]
R = 8.314    # ideal gas constant SI units
T =  273.
sigma = 0.073
A = 4*Mw*sigma/(R*T*rhow)

# Convert diameters to m
Dp_lower = Dp_lower*1e-6
Dp_upper = Dp_upper*1e-6
Dp = Dp*1e-6

# critical super saturation for each diameter
Sc_lower = np.exp( ((4*A**3)/(27*Dp_lower**3*tkappa))**0.5)
Sc_upper = np.exp( ((4*A**3)/(27*Dp_upper**3*tkappa))**0.5)

Sc_lower = (Sc_lower - 1.)*100.     # THIS IS NOW A PERCENT
Sc_upper = (Sc_upper - 1.)*100.

# This loop is poor programing, but you get the idea
Samb = 0.2   # CCN at 0.2% supersaturation
ccns1 = np.zeros((nlevs,len(lat),len(lon)))
for l in range(0, nlevs):
   for la in range(0, len(lat)):
      for lo in range(0, len(lon)):
         ccn1 = 0
         for i in range(0, nbins):
            if Sc_lower[i,l,la,lo] < Samb:
               ccn1 = ccn1 + aeroNum[i,l,la,lo]
            elif Sc_upper[i,l,la,lo] < Samb:
               ccn1 = ccn1 + aeroNum[i,l,la,lo]*(Samb - Sc_upper[i,l,la,lo])/(Sc_lower[i,l,la,lo]-Sc_upper[i,l,la,lo])

            ccns1[l,la,lo] = ccn1    # CCN 0.2% [m-3]

            
ccns1 = ccns1 * 1e-6   #[cm-3]
