; This code is intended for use in GEOS-Chem-TOMAS output
; Note: This code will be obsolete by model version 11.02
; Code converts bpch file to monthly netCDF files
; Starting with v11.02, GC will not output bpch files

; Authors: Sal Farina, Jeff Pierce, updates - Jack Kodros

pro split_proc_all
   ; Output directory
   ;Dir = './'

  ; output file name
  orunname=['gcT15']

  ; Model years and months
  years=[2016]
  months=[1,2,3,4,5,6,7,8,9,10,11,12]

  ;lenl=size(locs,/DIMENSIONS)-1
  ;for l = 0,lenl[0],1 do begin
  ;   loc=locs[l]

  ; Loop over years and months
  leny=size(years,/DIMENSIONS)-1
  for y = 0,leny[0],1 do begin
     year=years[y]
     lenm=size(months,/DIMENSIONS)-1
     for m = 0,lenm[0],1 do begin
        month=months[m]

        ; Format date correctly
        date=year*long(10000)+month*long(100)+long(1)
        print,date
        datestr = STRTRIM(date, 2)

        ; Call bpch_sep_sal -- separates bpch files into months
        ; Note - TOMAS files are sometimes too large to read all at once
        Bpch_Sep_Sal,'trac_avg.geos5_4x5_TOMAS15.2015120100','ctm.'+datestr+'.bpch',Tau0=nymd2tau(date)
        InFile = 'ctm.'+datestr+'.bpch'
        print,InFile

        OutFile = orunname+'_'+datestr+'.nc'
        print,OutFile
        ctm_cleanup

        ; Convert tracers to netCDF file
        bpch2coards, InFile, OutFile, DIAGN='IJ-AVG-$'

        ; Convert any other diagnostics you may be interested in
        ;OutFile2 = orunname+'_'+datestr+'_nuc.nc'
        ;bpch2coards, InFile, OutFile2, DIAGN='TOMAS-3D'
        ;OutFile3 = orunname+'_'+datestr+'_PORL-L.nc'
        ;bpch2coards, InFile, OutFile3, DIAGN='PORL-L=$'
        ;OutFile9 = orunname+'_'+datestr+'_CHEM-L.nc'
        ;bpch2coards, InFile, OutFile9, DIAGN='CHEM-L=$'
        ctm_cleanup

     endfor
   endfor

end
